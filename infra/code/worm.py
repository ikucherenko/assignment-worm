import sys

from pexpect import pxssh

SSH_LOGIN = "root"
SSH_PASSWD = "password"
SSH_SESSION_TIMEOUT = 5


def print_usage():
    """
    Prints hint for application
    :return: None
    """
    print("Usage: 'python worm.py {COMMAND} {HOSTS LIST}'")
    print("Example: python worm.py hostname 10.0.0.1,10.0.0.2")
    
    
def command_execute(command: str, ip: list):
    """
    Trying to connect to get remote shell and execute command
    :param command: String representation of command for remote executing
    :param ip: target host IP
    :return: None
    """
    session = pxssh.pxssh(timeout=SSH_SESSION_TIMEOUT)
    try:
        session.login(ip, SSH_LOGIN, SSH_PASSWD)
        session.sendline(command)
        session.prompt()
        result = session.before.splitlines()[1]
        print("{0} - returns {1}".format(ip, result.decode('utf-8')))
        session.logout()
    except pxssh.ExceptionPxssh as exp:
        print("{0} - host unreachable".format(ip))
    except Exception as ex:
        print(ex)


def main(args: list):
    """
    Main function
    :param args:
    :return:
    """
    help = ("-h", "--help")
        
    if args[0] in help:
        print_usage()
        sys.exit(0)

    if len(args) != 2:
        print("Incorrect input format")
        print_usage()
        sys.exit(1)

    command, ip_pool = args
    ip_pool = ip_pool.split(",")

    for ip in ip_pool:
        command_execute(command, ip)
       
    
if __name__ == "__main__":
    main(sys.argv[1:])
